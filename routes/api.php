<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Api', 'middleware' => 'api'], function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('system/init', 'SettingController@init');
    Route::group(['prefix' => 'password'], function () {
        Route::post('create', 'Auth\PasswordResetController@create')->name('auth.create');
        Route::get('find/{token}', 'Auth\PasswordResetController@find')->name('auth.find');
        Route::post('reset', 'Auth\PasswordResetController@reset')->name('auth.reset');
    });
    Route::get('crawl', 'EmployeeControler@crawl');
});

Route::group([
    'namespace' => 'Api',
    'middleware' => 'api'
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::post('logout', 'Auth\AuthController@logout');
        Route::post('images/upload', 'UploadImageController@uploadImage');
        Route::post('images/delete', 'UploadImageController@deleteImage');
        Route::group(['prefix' => 'language'], function () {
            Route::post('detailLanguage', 'LanguageController@detailLanguage')->name('language.detail');
            Route::post('saveLanguage', 'LanguageController@saveLanguage')->name('language.save');
            Route::post('searchLanguage', 'LanguageController@searchLanguage')->name('language.search');
            Route::post('activeLanguage', 'LanguageController@getActiveLanguage')->name('language.active');
            Route::post('saveLanguageStatic', 'LanguageController@saveLanguageStatic')->name('language.saveLanguageStatic');
            Route::post('searchLanguageStatic', 'LanguageController@searchLanguageStatic')->name('language.searchLanguageStatic');
            Route::post('saveLanguageStaticByLang', 'LanguageController@saveLanguageStaticByLang')->name('language.saveLanguageStaticByLang');
            Route::post('deleteLanguage', 'LanguageController@deleteLanguage')->name('language.delete');
        });
        Route::group(['prefix' => 'setting'], function () {
            Route::post('getSetting', 'SettingController@getSetting')->name('setting.get');
            Route::post('saveSetting', 'SettingController@saveSetting')->name('setting.save');
        });
    });
});
Route::group([
    'middleware' => 'api'
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::group(['prefix' => 'contacts'], function () {
            Route::post('search', 'ContactsController@index');
            Route::post('add', 'ContactsController@store');
            Route::post('detail', 'ContactsController@show');
            Route::post('update', 'ContactsController@update');
        });



        Route::post('role/detailRole', 'Api\RoleController@detailRole');
        Route::post('role/saveRole', 'Api\RoleController@saveRole');
        Route::post('role/savePermissionRole', 'Api\RoleController@savePermissionRole');
        Route::post('role/searchRole', 'Api\RoleController@index');
        Route::post('role/deleteRole', 'Api\RoleController@deleteRole');

        Route::post('permission', 'Api\Auth\AuthController@getPermission');
        Route::post('saveUser', 'Api\Auth\AuthController@saveUser');
        Route::post('deleteUser', 'Api\Auth\AuthController@deleteUser');
        Route::post('listUser', 'Api\Auth\AuthController@listUser');
        Route::post('detailUser', 'Api\Auth\AuthController@detailUser');
        Route::post('history/login', 'Api\Auth\AuthController@history');
        Route::post('changePass', 'Api\Auth\AuthController@changePass');
        Route::post('deleteCache', 'Api\SettingController@deleteCache');
    });
});

