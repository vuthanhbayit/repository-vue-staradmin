<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset(getConfig('config_icon')) }}" rel="icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravue Dashboard</title>
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" />
    <script>
        window.appSettings = {
            apiUrl: '{{ $data['apiUrl'] }}',
            debug: true,
            version: '1.0.0',
            mode: '{{ $data['config_mode'] }}'
        };
    </script>
</head>
<body>
<div id="app"></div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
