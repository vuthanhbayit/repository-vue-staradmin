import axios from '../../core/plugins/http';

export const getListSetting = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/setting/getSetting',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_SETTING", response.data);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_SETTING", null);
            return reject(err);
        })
    })
};
export const saveSetting = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/setting/saveSetting',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
