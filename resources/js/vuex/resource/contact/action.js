import axios from '../../../core/plugins/http';

export const getListContact = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/contacts/search',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_CONTACT", response.data);
            return resolve(response.data);
        }).catch(err =>{
            commit("GET_LIST_CONTACT", null);
            return reject(err);
        })
    })
};

export const getDetailContact = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/contacts/detail',
                ...opts
            }
        }).then(response => {
            commit("GET_DETAIL_CONTACT", response.data);
            return resolve(response);
        }).catch(err => {
            commit("GET_DETAIL_CONTACT", null);
            return reject(err);
        })
    })
};

export const updateContact = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/contacts/update',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};