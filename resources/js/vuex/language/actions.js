﻿import axios from '../../core/plugins/http';

export const getListLanguages = ({ commit }, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/language/searchLanguage',
                ...opts
            }
        }).then(response => {
            commit("GET_LIST_LANGUAGE", response);
            return resolve(response);
        }).catch(err => {
            commit("GET_LIST_LANGUAGE", null);
            return reject(err);
        });
    })
};
export const getActiveLanguages = ({ commit }) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/language/activeLanguage',
            }
        }).then(response => {
            commit("GET_LIST_ACTIVE_LANGUAGE", response);
            return resolve(response);
        }).catch(err => {
            commit("GET_LIST_ACTIVE_LANGUAGE", null);
            return reject(err);
        });
    })
};
export const getDetailLanguage = ({ commit }, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/language/detailLanguage',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return reject(err);
        });
    })
};
export const saveLanguage = ({ commit }, opts) => {
    return new Promise((resolve, reject) => {
        axios({
            data: {
                url: '/api/language/saveLanguage',
                ...opts
            }
        }).then((response) => {
            return resolve(response);
        }).catch(err => {
            return reject(err);
        });
    })
};
export const getListLanguageStatic = ({ commit }) => {
    return new Promise((resolve, reject) => {
        return axios({
            data: {
                url: '/api/language/searchLanguageStatic',
            }
        }).then(response => {
            commit("GET_LANGUAGE_STATIC", response);
            return resolve(response);
        }).catch(err => {
            commit("GET_LANGUAGE_STATIC", null);
            return reject(err);
        });
    })
};
export const saveLanguageStatic = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/language/saveLanguageStatic',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const saveLanguageStaticByLang = ({commit}, data) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/language/saveLanguageStaticByLang',
                static: data
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteLanguage = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/language/deleteLanguage',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
