import * as actions from './actions';

const store = {
    state: {
        listModule: null,
    },
    mutations: {
        GET_LIST_MODULE: (state, payload) => {
            state.listModule = payload.data;
        },
    },
    actions,
    getters: {
        listModule: state => state.listModule,
    }
};
export default store;
