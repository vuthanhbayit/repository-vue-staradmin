import Vue from 'vue';
import VueRouter from 'vue-router';

const _import = require('./_import_sync');
import App from '../components/app.vue';
import AppBasic from '../components/app-basic.vue';
import store from '../vuex/index';
import ENUM from "../../config/enum";
import CONSTANTS from "../core/utils/constants"

Vue.use(VueRouter);
let _routers = [
    {
        path: '/',
        component: App,
        children: [
            {
                path: '/',
                component: _import('user/index'),
                meta: {
                    requiresAuth: true,
                }
            },
            {
                path:'/logout',
                component: _import('auth/logout'),
                meta:{
                    requiresAuth: true,
                }
            },
            {
                path:'/user',
                component: _import('user/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/role',
                component: _import('role/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/language',
                component: _import('languages/language/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/languageKey',
                component: _import('languages/languageKey/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/setting',
                component: _import('setting/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/contact',
                component: _import('resource/contact/index'),
                meta:{
                    requiresAuth: true,
                }
            },
        ]
    },
    {
        path: '/',
        component: AppBasic,
        children: [
            {
                path: '/login',
                component: _import('auth/login')
            }
        ]
    },
    {
        path: '*',
        component: App,
        children: [
            {
                path: '/',
                component: _import('_shared/404')
            },
            {
                path: '/not-allow-access',
                component: _import('_shared/not-allow-access')
            }
        ]
    }
];
const router = new VueRouter({
    errorHandler(to, from, next, error) {
    },
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return {x: 0, y: 0}
        }
    },
    routes: _routers
});
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        try {
            if (!process.env.VUE_ENV) {
                if (localStorage.getItem(CONSTANTS.ACCESS_TOKEN)) {
                    if(to.meta.permission){
                        if(localStorage.getItem(CONSTANTS.ACCESS_PERMISSION).includes(to.meta.permission)) {
                            return next();
                        }else{
                            router.push({path: '/not-allow-access'});
                        }
                    }else{
                        return next();
                    }
                } else {
                    router.push({path: '/login', query: {returnUrl: to.fullPath}});
                }
            } else {
                if(to.meta.permission){
                    if(localStorage.getItem(CONSTANTS.ACCESS_PERMISSION).includes(to.meta.permission)) {
                        return next();
                    }else{
                        router.push({path: '/not-allow-access'});
                    }
                }else{
                    return next();
                }
            }
        } catch (e) {
            return next();
        }
    } else {
        return next();
    }
});

export default router;
