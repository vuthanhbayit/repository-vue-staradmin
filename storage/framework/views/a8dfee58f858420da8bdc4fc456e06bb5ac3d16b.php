<?php $__env->startSection('banner'); ?>
    <?php if(count($data['banner']) > 0): ?>
        <div class="banner-horizontal pt-header">
            <div class="swiper-container wp-slide-banner" id="wp-slide-banner">
                <div class="swiper-wrapper slide-banner">
                    <?php $__currentLoopData = $data['banner']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="swiper-slide item-slide">
                            <div class="img-thumb">
                                <a href="<?php echo e($item->link); ?>" class="item-slide-banner">
                                    <img src="<?php echo e(asset($item->image)); ?>" alt="" class=" image-cover"/>
                                </a>
                            </div>
                            <div class="info-banner container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="overlay-banner">
                                            <h3 class="title text-white"><?php echo e($item->title); ?></h3>
                                            <div class="text text-white subtitle mb-3">
                                                <?php echo $item->content; ?>
                                            </div>
                                            <a href="<?php echo e($item->link); ?>"
                                               class="readmore text-white"><?php echo e(getLanguage('ViewMore')); ?></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination pagination-banner"></div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php if(count($breadcrumbs)): ?>
    <div class="wp-breadcumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="list-breadcumb">
                        <?php $__currentLoopData = $breadcrumbs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="<?php echo e($breadcrumb['href']); ?>"><span><?php echo e($breadcrumb['text']); ?></span></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php
        echo $content_top;
    ?>
    <?php if(!empty($data['category'])): ?>
    <div class="services-sakura order-study">
        <div class="panel-body-services">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="panel-body-services pd-sakura">
                            <div class="row">
                                <div class="col-12">
                                    <div class="panel-head-content-news panel-head-general text-center">
                                        <div class="row inner">
                                            <div class="col-lg-10 col-md-10 col-sm-12 col-12 m-auto">
                                                <div class="content-news">
                                                    <h2 class="heading-2"><?php echo $data['category']['title'] ?></h2>
                                                    <div class="rule"></div>
                                                    <div class="intro heading-3 content-custom" id="content-description">
                                                        <?php echo $data['category']['description'] ?>
                                                    </div>
                                                </div>                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if(count($data['goodPoint']) > 0): ?>
        <div class="big-function-sakura pd-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                            <div class="swiper-container swiper-function-sakura" id="swiper-function">
                                <ul class=" wrap-item-function text-center swiper-wrapper">
                                    <?php $__currentLoopData = $data['goodPoint']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="text-center swiper-slide">
                                            <div class="item-function">
                                                <div class="funtion text-center">
                                                    <div class="image"><img src="<?php echo e(asset($item['icon'])); ?>" alt="" class="icon-function-sakura"></div>
                                                    <div class="text heading-3"><?php echo e($item['content']); ?></div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="tab-content">
        <div class="hot-package-sakura pd-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-hot-package">
                            <div class="panel-body-hot-package">
                                <ul class="list row">
                                    <?php $__currentLoopData = $data['product']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="col-lg-12 col-md-6 col-sm-6 col-12">
                                            <div class="article-sakura">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-12 ">
                                                        <div class="img-thumb">
                                                            <a href="/p<?php echo e($item['id']); ?>" class="image">
                                                                <img src="<?php echo e(asset($item['image'])); ?>" alt=""
                                                                     class="img-fluid image-cover">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                                                        <div class="info">
                                                            <div class="clearfix"></div>
                                                            <a href="/p<?php echo e($item['id']); ?>">
                                                                <div class="title-package heading-3 mb-2"><?php echo e($item['title']); ?></div>
                                                            </a>
                                                            <div class="info-package row">
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-4 col-md-12 col-12 col-12">
                                                                            <div
                                                                                class="wp-item-package-details heading-3">
                                                                                <span class="icon"><img
                                                                                        src="/images/icon-package-1.png"
                                                                                        alt=""></span>
                                                                                <span
                                                                                    class="text"><?php echo e(getLanguage('CodeClient')); ?>: <?php echo e($item['code']); ?></span>
                                                                            </div>
                                                                            <div
                                                                                class="wp-item-package-details heading-3">
                                                                                <span class="icon"><img
                                                                                        src="/images/icon-package-2.png"
                                                                                        alt=""></span>
                                                                                <span class="text"><?php echo e(getLanguage('DayNumberClient')); ?>: <?php echo e($item['dayNumber']); ?></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-12 col-12 col-12">
                                                                            <div
                                                                                class="wp-item-package-details heading-3">
                                                                                <span class="icon"><img
                                                                                        src="/images/icon-package-3.png"
                                                                                        alt=""></span>
                                                                                <span
                                                                                    class="text"><?php echo e(getLanguage('DepartAddressClient')); ?>: <?php echo e($item['departAddress']); ?></span>
                                                                            </div>
                                                                            <div
                                                                                class="wp-item-package-details heading-3">
                                                                                <span class="icon"><img
                                                                                        src="/images/icon-package-4.png"
                                                                                        alt=""></span>
                                                                                <span class="text"><?php echo e(getLanguage('SeatsExistClient')); ?>: <?php echo e($item['seatsExist']); ?></span>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="col-lg-4 col-md-12 col-12 col-12 wp-sakura">
                                                                            <div class="wp-price mb-3">
                                                                                <span
                                                                                    class="price text-color"><?php echo e($item['price']); ?><?php echo e(getConfig('config_currency')); ?></span>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="wp-view-package">
                                                                                <a href="/p<?php echo e($item['id']); ?>"
                                                                                   class="view-more-package"><?php echo e(getLanguage('ViewMore')); ?></a>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                                <div class="view text-center">
                                    <nav>
                                        <ul class="pagination clearfix">
                                            <?php echo $data['product']->links(); ?>

                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="request-quote ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-10 col-sm-12 col-12 m-auto">
                            <div class="content-request-quote">
                                <div class="panel-head-travel-package panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 m-auto">
                                            <h2 class="heading-2"><?php echo e(getLanguage('TitleRequestQuickQuote')); ?></h2>
                                            <div class="rule"></div>
                                            <p class="intro heading-3 mb-30"><?php echo e(getLanguage('DescRequestQuickQuote')); ?></p>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-12 m-auto">
                                                    <a href="/quickQuote" class="btn-detail"><?php echo e(getLanguage('ViewQuickQuote')); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo $content_bottom;
    ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <?php
        echo $footer;
    ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\xampp\htdocs\sakura.amela\resources\views/client/page/category.blade.php ENDPATH**/ ?>