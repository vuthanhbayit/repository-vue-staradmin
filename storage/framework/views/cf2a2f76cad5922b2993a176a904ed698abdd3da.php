<header class="pc-header" id="pc-header">
    <div class="topbar" id="topbar">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wrap-topbar d-flex">
                        <ul class="list-flag ml-auto">
                            <li class="align-items-center">
                                <div class="wrap-price-head">
                                    <a href="/quickQuote">
                                        <span class="icon"><img src="/images/price.png" alt=""></span>
                                        <span class="text"><?php echo e(getLanguage('QuickQuote')); ?></span>
                                    </a>
                                </div>
                            </li>
                            <?php $languages = getActiveLanguages(); ?>
                            <?php if(!empty($languages)): ?>
                                <form action="/languages" method="POST" enctype="multipart/form-data"
                                      id="form-language">
                                    <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <button class="btn btn-link btn-block language-select" type="button"
                                                    name="<?php echo e($item['code']); ?>"><img
                                                    src="<?php echo e(asset($item['flagIcon'])); ?>"
                                                    alt="<?php echo e($item['name']); ?>"
                                                    title="<?php echo e($item['name']); ?>"/></button>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <input type="hidden" name="code" value=""/>
                                </form>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="upper-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wrap-content-menu-sakura d-flex flex-wrap justify-content-between align-items-center">
                        <?php $__currentLoopData = getMenu('top'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php
                                echo $item;
                            ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<?php /**PATH E:\xampp\htdocs\Amela\sakura.amela\resources\views/client/share/menu.blade.php ENDPATH**/ ?>