<?php $__env->startComponent('mail::message'); ?>
# Hello!

Your <?php echo e(config('app.name')); ?> account logged in from a new device.

> **Account:** <?php echo e($account->email); ?><br>
> **Time:** <?php echo e($time->toCookieString()); ?><br>
> **IP Address:** <?php echo e($ipAddress); ?><br>
> **Browser:** <?php echo e($browser); ?>


If this was you, you can ignore this alert. If you suspect any suspicious activity on your account, please change your password.

Regards,<br><?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
<?php /**PATH C:\xampp\htdocs\sakura.amela\resources\views/vendor/authentication-log/emails/new.blade.php ENDPATH**/ ?>