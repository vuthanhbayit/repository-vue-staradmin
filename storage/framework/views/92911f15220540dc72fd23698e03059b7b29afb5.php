<button onclick="topFunction()" id="toppage-btn" title="Go to top"><img src="/images/top-page.png" alt=""
                                                                        class="btn-toppage"></button>
<footer class="footer align-self-end" id="footer">
    <div class="top-footer pd-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-top-footer">
                        <div class="row">
                            <?php if(!empty($modules['footer1'])): ?>
                                <div class="col-lg-5 col-md-12 col-sm-6 col-12 mb-30">
                                    <div class="content-company-sakura list-menu-ft">
                                        <?php $__currentLoopData = $modules['footer1']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($item->title)): ?>
                                                <div class="name-company-sakura"> <?php echo $item->title ?></div>
                                            <?php endif; ?>
                                            <?php if(isset($item->content)): ?>
                                                <?php echo $item->content; ?>

                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if(!empty($modules['footer2'])): ?>
                                <div class="col-lg-2 col-md-4 col-sm-6 col-12 mb-30">
                                    <div class="block-menu-footer list-menu-ft">
                                        <?php $__currentLoopData = $modules['footer2']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($item->title)): ?>
                                                <div class="name-company-sakura"> <?php echo $item->title ?></div>
                                            <?php endif; ?>
                                            <?php if(isset($item->content)): ?>
                                                <?php echo $item->content; ?>

                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if(!empty($modules['footer3'])): ?>
                                <div class="col-lg-2 col-md-4 col-sm-6 col-12 mb-30">
                                    <div class="block-menu-footer list-menu-ft">
                                        <?php $__currentLoopData = $modules['footer3']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($item->title)): ?>
                                                <div class="name-company-sakura"> <?php echo $item->title ?></div>
                                            <?php endif; ?>
                                            <?php if(isset($item->content)): ?>
                                                <?php echo $item->content; ?>

                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if(!empty($modules['footer4'])): ?>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-30">
                                    <div class="block-menu-footer list-menu-ft">
                                        <?php $__currentLoopData = $modules['footer4']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($item->title)): ?>
                                                <div class="name-company-sakura"> <?php echo $item->title ?></div>
                                            <?php endif; ?>
                                            <?php if(isset($item->content)): ?>
                                                <?php echo $item->content; ?>

                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="upper-footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-upper-ft">
                        <div class="copyright text-white mr-auto">Copyright © 2019 <span
                                class="text-color">Sakura</span></div>
                        <div class="wp-social">
                            <ul class="list-social">
                                <li><a href="<?php echo e(getConfig('config_facebook')); ?>" class="social"><i class="fab fa-facebook-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/js/swiper.min.js"></script>
<script type="text/javascript" src="/js/style.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="/js/wow.min.js"></script>
<?php /**PATH C:\xampp\htdocs\sakura.amela\resources\views/client/layouts/footer.blade.php ENDPATH**/ ?>