<div class="experience-sakura pd-sakura">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="content-experience-sakura">
                    <?php if($data['descriptions'] != NULL): ?>
                        <div class="panel-head-general mb-5 text-center">
                            <div class="row">
                                <div class="col-12 col-sm-10 m-auto">
                                    <h2 class="heading-2 "><?php echo e($data['descriptions']['title']); ?></h2>
                                    <div class="rule"><?php echo e($data['descriptions']['description']); ?></div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php
                        $category = ''; $content = '';
                    ?>
                    <?php $__currentLoopData = $blog; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($item['category'] != null): ?>
                            <?php
                                if ($i == 0){
                                    $active = 'active';
                                }else{
                                    $active = '';
                                }
                                $category .= '<li class="col">
                                                    <a class="item '.$active.'" id="pills-home-tab" data-toggle="pill"
                                                       href="#experience-'.$i.'" role="tab" aria-selected="true">
                                                        <div class="item-experience-sakura bg-sakura align-items-center '.$active.'">
                                                            <div class="img-thumb">
                                                                <div class="img-experience"><img
                                                                        src="'.asset($item['category']['icon']).'" alt="'. $item['category']['title'] .'"></div>
                                                            </div>
                                                            <div class="text heading-3">'. $item['category']['title'] .'</div>
                                                        </div>
                                                    </a>
                                                </li>';
                                $content .='<div class="tab-pane fade animated fadeIn '.$active.'" id="experience-'.$i.'" role="tabpanel">
                                                <div class="panel-body-experience">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="row">';
                            $first =''; $liPost = '';
                            ?>
                            <?php $__currentLoopData = $item['post']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                    if($k == 0){
                                        $first ='<div class="col-lg-6 col-md-12 col-sm-12 wp-info-experience">
                                                     <div class="big-news-experience">
                                                         <div class="img-thumb img-flash">
                                                            <img src="'. asset($post['image']).'" alt="'.$post['title'].'" class="img-fluid image-cover ">
                                                         </div>
                                                         <div class="info">
                                                             <a href="/bp'.$post['id'].'"> <div class="intro heading-3">'.$post['title'].'</div> </a>
                                                         </div>
                                                     </div>
                                                 </div>';
                                    }else{
                                        $liPost .='<li class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                        <div class="img-shine">
                                                            <a href="/bp'.$post['id'].'"><img src="'. asset($post['image']).'" alt=""
                                                                        class="img-fluid image-cover"><p>'.$post['title'].'</p></a></div>
                                                   </li>';
                                    }
                                ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php
                                if(count($item['post']) >= 5){
                                    $viewMore = '<div class="view mx-auto mt-5">
                                                        <a href="/bc'. $item['category']['id'] .'" class="view-more heading-3">'.getLanguage('ViewMore').'</a>
                                                    </div>';
                                }else{
                                    $viewMore = '';
                                }
                                    $content .=  ''.$first.'<div class="col-lg-6 col-md-12 col-sm-12 wp-thumb-experience">
                                                                 <ul class="list row">
                                                                    '.$liPost.'
                                                                 </ul>
                                                             </div>
                                                        </div>
                                                    </div>
                                                    '.$viewMore.'
                                                </div>
                                            </div>
                                        </div>';
                            ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <div class="panel-body-experience-sakura">
                        <div class="panel-head-experience mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <ul class="nav nav-pills list-head-experience row" id="pills-tab" role="tablist">
                                        <?php echo $category; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content" id="pills-tabContent">
                            <?php echo $content; ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- experience-sakura -->
<?php /**PATH C:\xampp\htdocs\sakura.amela\resources\views/client/extension/module/blog_category.blade.php ENDPATH**/ ?>