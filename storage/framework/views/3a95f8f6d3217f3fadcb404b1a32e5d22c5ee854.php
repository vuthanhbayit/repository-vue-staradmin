<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link href="<?php echo e(asset(getConfig('config_icon'))); ?>" rel="icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravue Dashboard</title>
    <link href="<?php echo e(asset('/css/style.css')); ?>" rel="stylesheet" />
    <script>
        window.appSettings = {
            apiUrl: '<?php echo e($data['apiUrl']); ?>',
            debug: true,
            version: '1.0.0',
            mode: '<?php echo e($data['config_mode']); ?>'
        };
    </script>
</head>
<body>
<div id="app"></div>
<script src="<?php echo e(asset('js/app.js')); ?>"></script>
</body>
</html>
<?php /**PATH E:\xampp\htdocs\Learn laravel\repository-vue-staradmin\resources\views/admin/index.blade.php ENDPATH**/ ?>