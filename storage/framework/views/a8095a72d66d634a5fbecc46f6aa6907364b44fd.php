<?php if(count($data['product']) > 0): ?>
    <div class="travel-package pd-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-travel-package">
                        <div class="panel-head-travel-package panel-head-general text-center">
                            <div class="row">
                                <div class="col-12 col-sm-10 m-auto">
                                    <h2 class="heading-2"><?php echo e($data['descriptions']['title']); ?></h2>
                                    <div class="rule"></div>
                                    <p class="intro heading-3"><?php echo e($data['descriptions']['description']); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body-travel-package">
                        <ul class="list row">
                            <?php $__currentLoopData = $data['product']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li class="col-lg-4 col-md-4 col-sm-4 col-12">
                                    <article class="article-travel-package">
                                        <div class="img-thumb">
                                            <a href="/p<?php echo e($item['id']); ?>" class="image">
                                                <img src="<?php echo e(asset($item['image'])); ?>" alt="<?php echo e($item['title']); ?>"
                                                     class="img-fluid image-cover"></a>
                                        </div>
                                        <div class="info">
                                            <div class="info">
                                                <?php if($item['dayNumber']): ?>
                                                    <div class="meta">
                                                        <?php echo e($item['dayNumber']); ?> <?php echo e(getLanguage('StartFromClient')); ?> <?php echo e($item['price']); ?> <?php echo e(getConfig('config_currency')); ?>

                                                    </div>
                                                <?php endif; ?>
                                                <h3 class="title"><a
                                                            href="/p<?php echo e($item['id']); ?>"><?php echo e($item['title']); ?></a>
                                                </h3>
                                                <div class="view">
                                                    <a href="/p<?php echo e($item['id']); ?>"
                                                       class="view-more heading-3"><?php echo e(getLanguage('ViewMore')); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php endif; ?>
<?php /**PATH E:\xampp\htdocs\Learn laravel\laravel_vue\resources\views/client/extension/module/featured.blade.php ENDPATH**/ ?>