<?php $__env->startSection('banner'); ?>
    <?php if(count($banner) > 0): ?>
        <div class="banner-horizontal pt-header">
            <div class="swiper-container wp-slide-banner" id="wp-slide-banner">
                <div class="swiper-wrapper slide-banner">
                    <?php $__currentLoopData = $banner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="swiper-slide item-slide">
                            <div class="img-thumb">
                                <a href="<?php echo e($item->link); ?>" class="item-slide-banner ">
                                    <img src="<?php echo e(asset($item->image)); ?>" alt="" class="image-cover"/>
                                </a>
                            </div>
                            <div class="info-banner container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="overlay-banner">
                                            <h3 class="title text-white"><?php echo e($item->title); ?></h3>
                                            <div class="text text-white subtitle mb-3">
                                                <?php echo $item->content ?>
                                            </div>
                                            <a href="<?php echo e($item->link); ?>"
                                               class="readmore text-white"><?php echo e(getLanguage('ViewMore')); ?></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination pagination-banner"></div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php if(count($breadcrumbs) > 0): ?>
        <div class="wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            <?php $__currentLoopData = $breadcrumbs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><a href="<?php echo e($breadcrumb['href']); ?>"><span><?php echo e($breadcrumb['text']); ?></span></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php
        echo $content_top;
    ?>
    <div class="reviews travel-guide-sakura pd-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-travel-guide">
                        <div class="panel-head-travel-package panel-head-general text-center">
                            <div class="row">
                                <div class="col-12 m-auto">
                                    <h2 class="heading-2"><?php echo e(getLanguage('TitleReviewsClient')); ?></h2>
                                    <div class="rule"></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body-travel-guide">
                            <ul class="list">
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <article class="article-travel-guide row">
                                            <div class="col-lg-4 col-md-5 col-sm-5 col-12">
                                                <div class="img-thumb">
                                                    <a href="/rv<?php echo e($item['id']); ?>" class=" image img-flash">
                                                        <img src="<?php echo e(asset($item['image'])); ?>"
                                                             alt="<?php echo e($item['title']); ?>"
                                                             class="img-fluid w-100 image-cover">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-7 col-sm-7 col-12">
                                                <div class="info">
                                                    <h3 class="heading-3 title line-2">
                                                        <a href="/rv<?php echo e($item['id']); ?>" title=""
                                                           class="text-color"><?php echo e($item['title']); ?></a>
                                                    </h3>
                                                    <div
                                                        class="meta-date"><?php echo e(date("d-m-Y", strtotime($item['created_at']))); ?></div>
                                                    <div class="rule"></div>
                                                    <div
                                                        class="intro heading-3 line-4">
                                                        <?php if($item['description'] != null || trim($item['description']) != ''): ?>
                                                            <?php echo shorten_string($item['description'], 50); ?>

                                                        <?php else: ?>
                                                            <?php echo shorten_string($item['content'], 50); ?>

                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="view">
                                                        <a href="/rv<?php echo e($item['id']); ?>"
                                                           class="view-more heading-3"><?php echo e(getLanguage('ViewMore')); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                            <div class="view text-center">
                                <nav>
                                    <ul class="pagination clearfix">
                                        <?php echo $data->links(); ?>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo $content_bottom;
    ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <?php
        echo $footer;
    ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\xampp\htdocs\sakura.amela\resources\views/client/page/reviews.blade.php ENDPATH**/ ?>