<?php $__env->startSection('banner'); ?>
    <?php if(count($banner) > 0): ?>
        <div class="banner-horizontal pt-header">
            <div class="swiper-container wp-slide-banner" id="wp-slide-banner">
                <div class="swiper-wrapper slide-banner">
                    <?php $__currentLoopData = $banner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="swiper-slide item-slide">
                            <div class="img-thumb">
                                <a href="<?php echo e($item->link); ?>" class="item-slide-banner ">
                                    <img src="<?php echo e(asset($item->image)); ?>" alt="" class="image-cover"/>
                                </a>
                            </div>
                            <div class="info-banner container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="overlay-banner">
                                            <h3 class="title text-white"><?php echo e($item->title); ?></h3>
                                            <div class="text text-white subtitle mb-3">
                                                <?php echo $item->content; ?>
                                            </div>
                                            <a href="<?php echo e($item->link); ?>"
                                               class="readmore text-white"><?php echo e(getLanguage('ViewMore')); ?></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination pagination-banner"></div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
        echo $content_top;
    ?>
    <?php if(count($breadcrumbs) > 0): ?>
        <div class="wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            <?php $__currentLoopData = $breadcrumbs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><a href="<?php echo e($breadcrumb['href']); ?>"><span><?php echo e($breadcrumb['text']); ?></span></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="about-campany-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-12 m-auto">
                            <div class="content-about-campany pd-sakura">
                                <div class="panel-head-about-campany panel-head-general text-center mb-30">
                                    <div class="row">
                                        <div class="col-12 col-sm-10 m-auto">
                                            <h2 class="heading-2">Giới thiệu công ty</h2>
                                            <div class="rule"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body-about-campany text-justify">
                                    <p class="intro heading-3">
                                        Được thành lập với tiêu chí trở thành một cầu nối du lịch giữa Việt Nam và Nhật Bản, giúp cho du khách có thể hiểu thêm về lịch sử - văn hóa của xứ sở Mặt trời mọc thông qua các gói du lịch hấp dẫn và mang đậm bản sắc riêng của xứ sở Phù Tang.
                                    </p>
                                    <p class="intro heading-3">
                                        Bên cạnh đó, công ty Sakura còn là đơn vị tiên phong hỗ trợ kết nối các buổi gặp mặt, chia sẻ kinh nghiệm cũng như trao đổi hướng đi mới nhằm xúc tiến hợp tác thương mại cho Doanh nghiệp 2 nước Việt Nam – Nhật Bản.
                                    </p>
                                    <p class="intro heading-3">
                                        Bằng sự chuyên nghiệp, tận tâm và luôn hướng đến sự hài lòng của khách hàng, công ty Sakura mong muốn đem đến cho khách du lịch Việt Nam những trải nghiệm thật thú vị trong những ngày tham quan Nhật Bản và lưu giữ những kỷ niệm đẹp về con người và thiên
                                        nhiên Nhật Bản.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="open-letter-sakura">
        <div class="panel-head-open-letter mb-30">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-10 m-auto">
                        <div class="big-wp-open-letter">
                            <div class="wp-head-open-letter panel-head-general text-center mb-30">
                                <h2 class="heading-2">Thư ngỏ</h2>
                                <div class="rule"></div>
                            </div>
                            <div class="wp-body-open-letter">
                                <div class="row text-center">
                                    <div class="col-lg-10 col-md-12 col-sm-12 col-12 m-auto bg-flower-sakura">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="img-thumb"><img src="<?php echo e(asset('uploads/source/founder.png')); ?>" alt="" class="img-fluid"></div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12 m-auto">
                                                <div class="info-founder-sakura">
                                                    <div class="name-founder mb-4">KEN ISHIDA </div>
                                                    <div class="intro-founder heading-3" >Tổng Giám đốc công ty cổ phần SAKURA</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-12 m-auto">
                            <div class="panel-body-open-letter  pd-sakura">
                                <div class="intro heading-3 text-justify">
                                    <p class="heading-3">Kính chào Quý khách hàng và Quý đối tác, </p>
                                    <p>
                                        Công ty Cổ phần SAKURA được thành lập vào ngày 16/05/2019 với tiêu chí trở thành một cầu nối hợp tác giao lưu văn hóa – thương mại giữa Việt Nam và Nhật Bản, nhằm cung cấp các chương trình tham quan nghiên cứu, chăm sóc y tế, học tập nâng cao kết hợp
                                        du lịch với chất lượng dịch vụ tốt cho khách hàng với phương châm đem lại “niềm tin trên mọi hành trình”.
                                    </p>
                                    <p>
                                        Với sự hỗ trợ và tạo điều kiện của các cơ quan ban ngành tại Nhật Bản và Đại sứ quán Việt Nam tại Nhật Bản cũng như các cơ quan ban ngành, các tổ chức hiệp hội trong và ngoài nước, đã tạo động lực cho SAKURA tự tin để làm tốt hơn nữa vai trò và sứ mệnh
                                        của mình.
                                    </p>
                                    <p>
                                        Để luôn nhận được niềm tin nơi khách hàng và sự hỗ trợ nhiệt tình từ các đối tác, công ty Sakura đặt mục tiêu phải liên tục đổi mới và cải thiện mình đồng thời chú trọng dịch vụ chăm sóc khách hàng nhằm mang đến sự hài lòng tối đa cho khách hàng.
                                    </p>
                                    <p>
                                        Để đạt được mục tiêu tăng trưởng công ty và các hoạt động phục vụ cộng đồng, ngoài sự đoàn kết, gắn bó, đồng lòng của tập thể cán bộ, nhân viên và lãnh đạo SAKURA, chúng tôi rất mong muốn nhận được sự hợp tác và hỗ trợ quý báu từ Quý khách hàng và Quý
                                        đối tác.
                                    </p>
                                    <p> Xin chân thành cảm ơn! </p>
                                    <p> Tổng Giám Đốc </p>
                                    <p> Ken Ishida </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo $content_bottom;
    ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <?php
        echo $footer;
    ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\Amela\sakura.amela\resources\views/client/page/introduction.blade.php ENDPATH**/ ?>