<?php if(count($data['blogPosts']) > 0): ?>
    <div class="guide-points pd-sakura bg-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-guide-points">
                        <div class="panel-head-travel-package panel-head-general text-center">
                            <div class="row">
                                <div class="col-12 col-sm-10 m-auto">
                                    <h2 class="heading-2 "><?php echo e($data['descriptions']['title']); ?></h2>
                                    <div class="rule"></div>
                                    <p class="intro heading-3"><?php echo e($data['descriptions']['description']); ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body-guide-points">
                            <ul class="list row">
                                <?php $__currentLoopData = $data['blogPosts']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="col-lg-4 col-md-4 col-sm-4 col-12">
                                        <article class="article-guide-points wow fadeInLeft">
                                            <div class="img-thumb">
                                                <a href="/bp<?php echo e($item['id']); ?>" class="image-cover image">
                                                    <img src="<?php echo e(asset($item['image'])); ?>" alt=""
                                                         class="img-fluid image-cover"></a>
                                            </div>
                                            <div class="info">
                                                <h3 class="title"><a href="/bp<?php echo e($item['id']); ?>"
                                                                     title="<?php echo e($item['title']); ?>"
                                                                     class="heading-3"><?php echo e($item['title']); ?></a>
                                                </h3>
                                            </div>
                                        </article>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php /**PATH C:\xampp\xampp\htdocs\sakura.amela\resources\views/client/extension/module/interesting.blade.php ENDPATH**/ ?>