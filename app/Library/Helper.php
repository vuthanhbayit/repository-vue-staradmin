<?php
if (!function_exists('handle')) {
    function handle($request)
    {
        $input = $request->all();
        if ($input) {
            array_walk_recursive($input, function (&$item) {
                $item = trim($item);
                $item = ($item == "") ? null : $item;
            });
            $request->merge($input);
        }
        return $request->all();
    }
}

if (!function_exists('getConfig')) {
    function getConfig($key)
    {
        $setting = new \App\Models\Setting();
        return $setting->getValue($key);
    }
}

if (!function_exists('getLanguage')) {
    function getLanguage($key)
    {
        $code = app()->getLocale();
        $lang = new \App\Models\LanguageStaticByLang();
        return $lang->getLanguageValue($key, $code);
    }
}

if (!function_exists('getActiveLanguages')) {
    function getActiveLanguages()
    {
        $language = new \App\Models\Language();
        $data = $language->getLanguages();
        return $data;
    }
}

if (!function_exists('getCurrentLang')) {

    function getCurrentLang()
    {
        $locale = request()->cookie('locale');
        if (!isset($locale)) {
            $locale = getConfig('config_language');
        }
        return $locale;
    }
}

if (!function_exists('shorten_string')) {
    function shorten_string($string, $words = 50)
    {
        $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
        $string = str_replace("\n", " ", $string);
        $array = explode(" ", $string);
        if (count($array) <= $words) {
            $retval = $string;
        } else {
            array_splice($array, $words);
            $retval = implode(" ", $array) . " ...";
        }
        return $retval;
    }
}
if (!function_exists('resizeImage')) {
    function resizeImage($originalImagePath, $newImagePath, $width = 50, $height = 50)
    {
        $name = preg_split('/[\/]+/', $originalImagePath);
        if (!file_exists('cache/' . $name[0] . '/' . $name[1])) {
            mkdir('cache/' . $name[0] . '/' . $name[1], 0777, true);
        }
        if (file_exists($originalImagePath)) {
            if (!file_exists($newImagePath)) {
                $image = imagecreatefromstring(file_get_contents($originalImagePath));
                $thumb_width = $width;
                $thumb_height = $height;
                $width = imagesx($image);
                $height = imagesy($image);
                $original_aspect = $width / $height;
                $thumb_aspect = $thumb_width / $thumb_height;
                if ($original_aspect >= $thumb_aspect) {
                    $new_height = $thumb_height;
                    $new_width = $width / ($height / $thumb_height);
                } else {
                    $new_width = $thumb_width;
                    $new_height = $height / ($width / $thumb_width);
                }
                $thumb = imagecreatetruecolor($thumb_width, $thumb_height);
                imagecopyresampled($thumb,
                    $image,
                    0 - ($new_width - $thumb_width) / 2,
                    0 - ($new_height - $thumb_height) / 2,
                    0, 0,
                    $new_width, $new_height,
                    $width, $height);
                imagejpeg($thumb, $newImagePath, 80);
            }
        }
    }
}
